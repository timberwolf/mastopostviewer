//
// account.js
// mastopostviewer
//
// Created by Timberwolf on 2020-07-27
//
// Handles the account name.
//

var account = {}

async function loadAccountName() {
	// let json = await fetch("actor.json")
	account.displayName = accountinfo.name
	let id = accountinfo.preferredUsername
	
	// build @username@instance string
	account.username = "@" + id
	// let instance = new URL(accountinfo.url).hostname
	// account.username = "@" + id + "@" + instance
	
	let displayNameField = document.getElementById("displayname")
	let usernameField = document.getElementById("username")
	displayNameField.textContent = account.displayName
	usernameField.textContent = account.username
}

window.addEventListener("DOMContentLoaded", (event) => {
	loadAccountName()
})
