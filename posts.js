//
// posts.js
// mastopostviewer
//
// Created by Timberwolf on 2020-07-27
//
// Handles reading posts from the archive.
//

let pagination = 500

let currentPostIndex = 0

let posts = [] // to hold all your posts, but not things you boosted and the like
let postsAndReplies = []

async function loadPosts(max) {
	let template = document.getElementById("post-template")
	
	for (let i = 0; max == null || i < max; i++) {
		let loadMoreContainer = document.getElementById("load-more-container")
		if (currentPostIndex >= posts.length) {
			// out of posts!
			loadMoreContainer.remove()
			break
		}
		
		let post = posts[currentPostIndex]
		
		let text = post.object.content
		
		let postDOM = template.content.cloneNode(true)
		console.log(postDOM)
		postDOM.querySelector(".displayname").textContent = account.displayName
		postDOM.querySelector(".username").textContent = account.username
		
		let postContentBox = postDOM.querySelector(".post-content")
		postContentBox.innerHTML = text
		
		let dateLink = postDOM.querySelector(".date-link")
		dateLink.textContent = post.published
		dateLink.href = post.object.url
		
		let privacy = postDOM.querySelector(".privacy")
		// if [to] contains users instead of an activitystreams thingy, it's a DM
		if (post.to.some((to) => /.*(activitystreams|followers).*/.test(to)) != true) {
			privacy.textContent = "✉︎"
		} else if (post.to.some((to) => /.*followers.*/.test(to)) == true) {
			privacy.textContent = "🔒︎"
		} else {
			privacy.textContent = "🌐︎"
		}
		
		let CWheader = postDOM.querySelector(".CWheader")
		if (post.object.summary != null) {
			postDOM.querySelector(".CW").textContent = post.object.summary
			postContentBox.style.display = "none"
			
			let showMoreButton = postDOM.querySelector(".show-more")
			showMoreButton.addEventListener("click", (event) => {
				// toggle show/hide
				if (postContentBox.style.display == "none") {
					postContentBox.style.display = "inline"
					showMoreButton.textContent = "Show Less"
				} else {
					postContentBox.style.display = "none"
					showMoreButton.textContent = "Show More"
				}
			})
		} else {
			// we don't need this
			CWheader.remove()
		}
		
		let postsContainer = document.getElementById("posts-container")
		postsContainer.insertBefore(postDOM, loadMoreContainer)
		
		currentPostIndex++
	}
}

window.addEventListener("DOMContentLoaded", (event) => {
	postsAndReplies = allPosts.orderedItems.filter((post) => {
		return post.type === "Create"
	})
	postsAndReplies.reverse()
	posts = postsAndReplies.filter((post) => {
		return post.object.inReplyToAtomUri == null
	})

	loadPosts(pagination)
})

document.getElementById("load-more").addEventListener("click", (event) => {
	loadPosts(pagination)
})
document.getElementById("load-all").addEventListener("click", (event) => {
	loadPosts(null)
})
